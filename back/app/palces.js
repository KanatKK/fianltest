const router = require("express").Router();
const Place = require("../models/Place");
const auth = require("../middleware/auth");
const config = require("../config");

router.get("/", async (req, res) => {
    try {
        const places = await Place.find().populate("user");
        res.send(places);
    } catch (e) {
        return res.status(400).send(e);
    }
});

router.get("/:id", async (req, res) => {
   try {
       const place = await Place.findById(req.params.id).populate("user");
       if (place) {
           res.send(place);
       } else {
           return res.status(400).send({error: "Place was not found"});
       }
   }  catch (e) {
       return res.status(400).send(e);
   }
});

router.post("/", [auth, config.upload.array('image', 1)], async (req, res) => {
    const placeData = req.body;

    if (placeData.agreement === "false") {
        return res.status(400).send({error: "You should agree with rules"});
    }
    if (!placeData.title) {
        return res.status(400).send({error: "Title field must be filled"});
    }
    if (!placeData.description) {
        return res.status(400).send({error: "Description field must be filled"});
    }
    if (!req.files[0]) {
        return res.status(400).send({error: "Image must be sent"});
    }
    if (req.files[0]) {
        placeData.image = req.files[0].filename;
    }

    const place = new Place(placeData);

    try {
        await place.save();
        res.send(place);
    } catch (e) {
        return res.status(400).send(e);
    }
});

router.patch("/:id", [auth, config.upload.array('image', 1)], async (req, res) => {
    let place = await Place.findById(req.params.id);
    if (req.files[0]) {
        place.gallery.push(req.files[0].filename);
    }
    const result = await Place.findByIdAndUpdate(req.params.id, place);
    if (result) {
        res.send({message: 'Success'});
    } else {
        res.sendStatus(404);
    }
});

router.patch("/review/:id", auth, async (req, res) => {
    let place = await Place.findById(req.params.id);
    place.reviews.push(req.body);
    place.qualityOfFoodArray.push(req.body.qualityOfFood);
    place.serviceQualityArray.push(req.body.serviceQuality);
    place.interiorArray.push(req.body.interior);

    let qualityOfFood = 0;
    for(let i = 0; i < place.qualityOfFoodArray.length; i++){
        Number(qualityOfFood += place.qualityOfFoodArray[i]);
    }

    let serviceQuality = 0;
    for(let i = 0; i < place.serviceQualityArray.length; i++){
        Number(serviceQuality += place.serviceQualityArray[i]);
    }

    let interiorArray = 0;
    for(let i = 0; i < place.interiorArray.length; i++){
        Number(interiorArray += place.interiorArray[i]);
    }

    place.qualityOfFood = Number(qualityOfFood / place.qualityOfFoodArray.length);
    place.serviceQuality = Number(serviceQuality / place.serviceQualityArray.length);
    place.interior = Number(interiorArray / place.interiorArray.length);

    place.overall = Number((place.qualityOfFood + place.serviceQuality + place.interior) / 3);

    const result = await Place.findByIdAndUpdate(req.params.id, place);

    if (result) {
        res.send({message: 'Success'});
    } else {
        res.sendStatus(404);
    }
});

module.exports = router;
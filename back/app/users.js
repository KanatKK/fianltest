const router = require("express").Router();
const User = require("../models/User");

router.post("/", async (req, res) => {
    try {
        const checkUser = await User.findOne({username: req.body.username});
        if (checkUser) {
            return res.status(400).send({error: "This username is already taken"});
        }
        if (!req.body.username) {
            return res.status(400).send({error: "Username field must be filled"});
        }
        if (!req.body.fullName) {
            return res.status(400).send({error: "FullName field must be filled"});
        }
        if (!req.body.password) {
            return res.status(400).send({error: "Password field must be filled"});
        }
        const user = new User(req.body);
        user.generateToken();
        await user.save();
        res.send(user);
    } catch (e) {
        return res.status(400).send(e);
    }
});

router.post("/sessions", async (req, res) => {
    const user = await User.findOne({username: req.body.username});
    if (!user) {
        return res.status(400).send({error: "Username not found"});
    }
    const isMatch = await user.checkPassword(req.body.password);
    if (!isMatch) {
        return res.status(400).send({error: "Password is wrong"});
    }
    user.generateToken();
    await user.save();
    res.send(user);
});

router.delete("/sessions", async (req, res) => {
    const token = req.get("Authorization");
    const success = {message: "Success"};

    if (!token) return res.send(success);
    const user = await User.findOne({token});

    if (!user) return res.send(success);

    user.generateToken();
    user.save();

    return res.send(success);
});

module.exports = router;
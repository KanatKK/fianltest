const mongoose = require("mongoose");
const {nanoid} = require("nanoid");
const config = require("./config");
const User = require("./models/User");
const Place = require("./models/Place");

mongoose.connect(config.db.url + "/" + config.db.name, {useNewUrlParser: true});
const db = mongoose.connection;

db.once("open", async () => {
    try {
        await db.dropCollection("users");
        await db.dropCollection("places");
    } catch (e) {
        console.log('Collections were not present, skipping drop...');
    }

    const [Rick, Morty, Admin] = await User.create({
        username: "user",
        fullName: "Rick",
        role: "user",
        token: nanoid(),
        password: "1234"
    }, {
        username: "user2",
        fullName: "Morty",
        role: "user",
        token: nanoid(),
        password: "4321"
    }, {
        username: "admin",
        fullName: "Admin",
        role: "admin",
        token: nanoid(),
        password: "12345678"
    });

    const [
        place1, place2, place3, place4,
        place5, place6, place7
    ] = await Place.create({
        title: "place 1",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam atque distinctio expedita facere magni, quisquam reprehenderit sunt! Aliquam, aliquid cumque dolor dolores doloribus et eum ipsa molestiae nisi provident reiciendis repellendus soluta temporibus velit voluptas. Debitis delectus est expedita fugiat illum laborum porro praesentium quaerat, quia quidem, quo repudiandae, unde!",
        image: "BWmqmsIYAuI3cZixVqQOv.jpg",
        user: Rick._id,
        gallery: [],
        qualityOfFoodArray: [4, 5, 3],
        serviceQualityArray: [5, 2, 4],
        interiorArray: [4, 3, 3],
        qualityOfFood: 4,
        serviceQuality: 3.6,
        interior: 3.3,
        overall: 3.6,
    }, {
        title: "place 2",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam atque distinctio expedita facere magni, quisquam reprehenderit sunt! Aliquam, aliquid cumque dolor dolores doloribus et eum ipsa molestiae nisi provident reiciendis repellendus soluta temporibus velit voluptas. Debitis delectus est expedita fugiat illum laborum porro praesentium quaerat, quia quidem, quo repudiandae, unde!",
        image: "Dm9pHzfmvYp9nXthyJkT0.jpg",
        user: Morty._id,
        gallery: [],
        qualityOfFoodArray: [4, 5, 3],
        serviceQualityArray: [2, 2, 4],
        interiorArray: [4, 5, 5],
        qualityOfFood: 4,
        serviceQuality: 2.6,
        interior: 4.6,
        overall: 3.7,
    }, {
        title: "place 3",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam atque distinctio expedita facere magni, quisquam reprehenderit sunt! Aliquam, aliquid cumque dolor dolores doloribus et eum ipsa molestiae nisi provident reiciendis repellendus soluta temporibus velit voluptas. Debitis delectus est expedita fugiat illum laborum porro praesentium quaerat, quia quidem, quo repudiandae, unde!",
        image: "evB4nRukN2k_NRw8em-7U.jpg",
        user: Morty._id,
        gallery: [],
        qualityOfFoodArray: [5, 5, 3],
        serviceQualityArray: [5, 2, 4],
        interiorArray: [4, 4, 5],
        qualityOfFood: 4.3,
        serviceQuality: 3.6,
        interior: 4.3,
        overall: 4,
    }, {
        title: "place 4",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus consequatur dignissimos distinctio, doloremque eligendi, ex iure magni neque nihil pariatur perferendis porro quae quam quasi recusandae sapiente tempora? Cumque doloremque doloribus numquam qui reiciendis saepe! Assumenda enim impedit suscipit. A consequatur ducimus laborum magni nihil, quam veniam. Adipisci amet architecto asperiores assumenda at autem commodi dicta ea est explicabo, harum illo inventore ipsa laboriosam laborum modi molestias mollitia nisi nostrum numquam odit porro possimus quam quasi quod rem repudiandae similique, sint soluta sunt tempore velit veritatis voluptates? Alias debitis expedita id necessitatibus voluptatem? Exercitationem fuga sequi vero. A, atque, eos?",
        image: "H2XP7iK1PGBRbwwxK-NPL.jpg",
        user: Rick._id,
        gallery: [],
        qualityOfFoodArray: [5, 5, 3],
        serviceQualityArray: [5, 5, 4],
        interiorArray: [4, 4, 5],
        qualityOfFood: 4.3,
        serviceQuality: 4.6,
        interior: 4.3,
        overall: 4.4,
    }, {
        title: "place 5",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus consequatur dignissimos distinctio, doloremque eligendi, ex iure magni neque nihil pariatur perferendis porro quae quam quasi recusandae sapiente tempora? Cumque doloremque doloribus numquam qui reiciendis saepe! Assumenda enim impedit suscipit. A consequatur ducimus laborum magni nihil, quam veniam. Adipisci amet architecto asperiores assumenda at autem commodi dicta ea est explicabo, harum illo inventore ipsa laboriosam laborum modi molestias mollitia nisi nostrum numquam odit porro possimus quam quasi quod rem repudiandae similique, sint soluta sunt tempore velit veritatis voluptates? Alias debitis expedita id necessitatibus voluptatem? Exercitationem fuga sequi vero. A, atque, eos?",
        image: "JpXyxGsO4wYHd4tU3cEsB.jpg",
        user: Rick._id,
        gallery: [],
        qualityOfFoodArray: [5, 5, 3],
        serviceQualityArray: [5, 5, 4],
        interiorArray: [4, 1, 5],
        qualityOfFood: 4.3,
        serviceQuality: 4.6,
        interior: 3.3,
        overall: 4,
    }, {
        title: "place 6",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus consequatur dignissimos distinctio, doloremque eligendi, ex iure magni neque nihil pariatur perferendis porro quae quam quasi recusandae sapiente tempora? Cumque doloremque doloribus numquam qui reiciendis saepe! Assumenda enim impedit suscipit. A consequatur ducimus laborum magni nihil, quam veniam. Adipisci amet architecto asperiores assumenda at autem commodi dicta ea est explicabo, harum illo inventore ipsa laboriosam laborum modi molestias mollitia nisi nostrum numquam odit porro possimus quam quasi quod rem repudiandae similique, sint soluta sunt tempore velit veritatis voluptates? Alias debitis expedita id necessitatibus voluptatem? Exercitationem fuga sequi vero. A, atque, eos?",
        image: "pmjU5Ln_uemTD6utTCtRP.jpg",
        user: Morty._id,
        gallery: [],
        qualityOfFoodArray: [5, 5, 3],
        serviceQualityArray: [5, 5, 4],
        interiorArray: [4, 5, 5],
        qualityOfFood: 4.3,
        serviceQuality: 4.6,
        interior: 4.3,
        overall: 4.5,
    }, {
        title: "place 7",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus consequatur dignissimos distinctio, doloremque eligendi, ex iure magni neque nihil pariatur perferendis porro quae quam quasi recusandae sapiente tempora? Cumque doloremque doloribus numquam qui reiciendis saepe! Assumenda enim impedit suscipit. A consequatur ducimus laborum magni nihil, quam veniam. Adipisci amet architecto asperiores assumenda at autem commodi dicta ea est explicabo, harum illo inventore ipsa laboriosam laborum modi molestias mollitia nisi nostrum numquam odit porro possimus quam quasi quod rem repudiandae similique, sint soluta sunt tempore velit veritatis voluptates? Alias debitis expedita id necessitatibus voluptatem? Exercitationem fuga sequi vero. A, atque, eos?",
        image: "UGZ9pp1PJgp290SQqjBSC.jpg",
        user: Rick._id,
        gallery: [],
        qualityOfFoodArray: [5, 5, 5],
        serviceQualityArray: [5, 5, 4],
        interiorArray: [4, 5, 5],
        qualityOfFood: 5,
        serviceQuality: 4.6,
        interior: 4.3,
        overall: 4.6,
    });

    db.close();
});


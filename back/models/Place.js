const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const PlaceSchema = new Schema({
    title: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    image: {
        type: String,
        required: true,
    },
    user: {
      type: Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    gallery: {
      type: Array,
    },
    qualityOfFoodArray: {
        type: Array,
    },
    serviceQualityArray: {
        type: Array,
    },
    interiorArray: {
        type: Array,
    },
    qualityOfFood: {
        type: Number,
    },
    serviceQuality: {
        type: Number,
    },
    interior: {
        type: Number,
    },
    overall: {
        type: Number,
    },
    reviews: {
        type: Array,
    }
});

const Place = mongoose.model("Place", PlaceSchema);
module.exports = Place;
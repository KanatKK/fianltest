import React from 'react';
import './Header.css';
import {useDispatch, useSelector} from "react-redux";
import {NavLink} from "react-router-dom";
import {logOutUser} from "../../store/actions";

const Header = () => {
    const user = useSelector(state => state.user.user);
    const dispatch = useDispatch();

    const logoutUserHandler = () => {
        dispatch(logOutUser());
    };

    return (
        <header>
            <div className="headerTitle">
                    <h4>
                        <NavLink className="headerLink" to={"/"}>
                            Places
                        </NavLink>
                    </h4>
            </div>
            <div className="userNav">
                {!user &&
                    <div className="registrationButtons">
                        <button className="regBtn">
                            <NavLink className="navLink" to={"/register"}>
                                Register
                            </NavLink>
                        </button>
                        /
                        <button className="loginBtn">
                            <NavLink className="navLink" to={"/login"}>
                                Login
                            </NavLink>
                        </button>
                    </div>
                }
                {user &&
                    <div className="loginUserNav">
                        <button
                            onClick={logoutUserHandler}
                            className="userBtn"
                        >
                            {user.fullName}
                        </button> /
                        <button className="userBtn">
                            <NavLink className="navLink" to={"/addNewPlace"}>
                                Add new place
                            </NavLink>
                        </button>
                    </div>
                }
            </div>
        </header>
    );
};

export default Header;
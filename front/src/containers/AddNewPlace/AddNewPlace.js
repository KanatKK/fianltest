import React, {useState} from 'react';
import './AddNewPlace.css';
import Header from "../../components/Header/Header";
import {useDispatch, useSelector} from "react-redux";
import {addPlace} from "../../store/actions";

const AddNewPlace = (props) => {
    const user = useSelector(state => state.user.user);
    const errors = useSelector(state => state.place.error);
    const dispatch = useDispatch();

    if (!user) {
        props.history.push("/");
    }

    const [state, setState] = useState({
        title: "",
        description: "",
        image: [],
        user: user._id,
        agreement: false,
    });

    const [image, setImage] = useState("");

    const changeInputData = event => {
        const name = event.target.name;
        const value = event.target.value;
        setState({
            ...state,
            [name]: value,
        });
    };

    const changeAgreement = () => {
        if (state.agreement === true) {
            setState({
                ...state,
                agreement: false,
            });
        } else {
            setState({
                ...state,
                agreement: true,
            });
        }
    };

    const handleImg = (event) => {
        if (event.target.files[0]) {
            const file = event.target.files[0];
            let reader = new FileReader();
            reader.onload = (e) => {
                setImage(e.target.result)
                setState({
                    ...state,
                    image: file,
                });
            };
            reader.readAsDataURL(file);
        }
    }

    const submitPlaceHandler = e => {
        e.preventDefault();
        const formData = new FormData();
        formData.append('title', state.title);
        formData.append('description', state.description);
        formData.append('user', state.user);
        formData.append('agreement', state.agreement);
        formData.append('image', state.image);
        dispatch(addPlace(formData));
        setState({
            ...state,
            title: "",
            description: "",
            image: [],
        });
        setImage("");
    };

    return (
        <div>
            <Header/>
            <div className="content">
                <h1>Add new place</h1>
                <input
                    type="text" placeholder="Add title"
                    className="regFields" name="title"
                    value={state.title} onChange={changeInputData}
                />
                <textarea
                    placeholder="Add description" className="placeDescription"
                    name="description" value={state.description} onChange={changeInputData}
                />
                <div className="imageBlock">
                    <input onChange={handleImg} type="file" className="inputForImages" id="inputImage"/>
                    <label className="inputImage" htmlFor="inputImage">Choose main image</label>
                </div>
                {image !== "" &&
                    <div className="choosenImage">
                        <img src={image} className="choosenImg" alt=""/>
                    </div>
                }
                <div className="checkBox">
                    By submitting this form, you agree to the rules of this site
                    <input type="checkbox" onClick={changeAgreement}/>
                </div>
                {errors && <span className="error">{errors}</span>}
                <button
                    className="addBtn"
                    onClick={submitPlaceHandler}
                >Submit new place</button>
            </div>
        </div>
    );
};

export default AddNewPlace;
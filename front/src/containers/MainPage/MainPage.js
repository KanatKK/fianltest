import React, {useEffect, useState} from 'react';
import './MainPage.css';
import Header from "../../components/Header/Header";
import {useDispatch, useSelector} from "react-redux";
import {getPlaces} from "../../store/actions";
import {NavLink} from "react-router-dom";

const MainPage = () => {
    const dispatch = useDispatch();
    const allPlaces = useSelector(state => state.place.places);
    const [places, setPlaces] = useState(null);
    useEffect(() => {
        dispatch(getPlaces());
    }, [dispatch]);
    useEffect(() => {
        if (allPlaces) {
            setPlaces(allPlaces)
        }
    }, [allPlaces, places])

    return (
        <div>
            <Header/>
            <div className="container">
                <div className="placesBlock">
                    {places && places.map((place) => {
                        return (
                            <div key={place._id} className="placeCard">
                                <div className="image">
                                    <NavLink to={"/place/" + place._id}>
                                        <img
                                            src={"http://localhost:8000/uploads/" + place.image}
                                            alt="" className="img"
                                        />
                                    </NavLink>
                                </div>
                                <h5 className="placeCardTitle">
                                    <NavLink to={"/place/" + place._id}>
                                        {place.title}
                                    </NavLink>
                                </h5>
                                <p className="placeGalleryArray">Photos: {place.gallery.length}</p>
                            </div>
                        )
                    })}
                </div>
            </div>
        </div>
    );
};

export default MainPage;

import React, {useEffect, useState} from 'react';
import "./Place.css";
import {useDispatch, useSelector} from "react-redux";
import {addReview, getPlace, updateGallery} from "../../store/actions";
import Header from "../../components/Header/Header";

const Place = props => {
    const dispatch = useDispatch()
    const place = useSelector(state => state.place.onePlace);
    const user = useSelector(state => state.user.user);

    useEffect(() => {
        dispatch(getPlace(props.match.params.id))
    }, [dispatch, props.match.params.id]);

    const [state, setState] = useState({image: []});
    const [image, setImage] = useState("");
    const [rating, setRating] = useState({
        description: "",
        qualityOfFood: 1,
        serviceQuality: 1,
        interior: 1,
    });
    console.log(place)
    const changeInputData = event => {
        const name = event.target.name;
        const value = event.target.value;
        setRating({
            ...rating,
            [name]: value,
        });
    };

    const handleImg = (event) => {
        if (event.target.files[0]) {
            const file = event.target.files[0];
            let reader = new FileReader();
            reader.onload = (e) => {
                setImage(e.target.result)
                setState({
                    ...state,
                    image: file,
                });
            };
            reader.readAsDataURL(file);
        }
    }

    const updateGalleryHandler = () => {
        const formData = new FormData();
        formData.append('image', state.image);
        dispatch(updateGallery(props.match.params.id, formData));
        setImage("");
    };

    const addReviewHandler = () => {
        dispatch(addReview(props.match.params.id, rating));
    };

    return (
        <div>
            <Header/>
            <div className="content">
                {place &&
                <div className="info">
                    <div className="textInfo">
                        <h1>
                            {place.title}
                        </h1>
                        <p>
                            {place.description}
                        </p>
                    </div>
                    <div className="placeImage">
                        <img src={"http://localhost:8000/uploads/" + place.image} alt="" className="placeImg"/>
                    </div>
                </div>
                }
                {place && place.gallery.length !== 0 &&
                    <>
                        <h3>Gallery: </h3>
                        <div className="gallery">
                            {place.gallery.map(img => {
                                return (
                                    <div key={img} className="galleryImage">
                                        <img
                                            src={"http://localhost:8000/uploads/" + img}
                                            alt="" className="galleryImg"
                                        />
                                    </div>
                                )
                            })}
                        </div>
                    </>
                }
                <div className="ratings">
                    <h3>Ratings: </h3>
                    <p>Overall: {place && place.overall}</p>
                    <p>Quality of food: {place && place.qualityOfFood}</p>
                    <p>Service quality: {place && place.serviceQuality}</p>
                    <p>Interior: {place && place.interior}</p>
                </div>
                <div className="reviews">
                    <h3>Reviews: </h3>
                    {user &&
                        <div className="addReviewBlock">
                            <textarea
                                onChange={changeInputData} value={rating.description}
                                className="reviewTextArea" name="description"
                            />
                            <label className="ratingSelector">
                                Quality of food:
                                <select value={rating.qualityOfFood} onChange={changeInputData} name="qualityOfFood">
                                    <option value={1}>1</option>
                                    <option value={2}>2</option>
                                    <option value={3}>3</option>
                                    <option value={4}>4</option>
                                    <option value={5}>5</option>
                                </select>
                            </label>
                            <label className="ratingSelector">
                                Service quality:
                                <select value={rating.serviceQuality} onChange={changeInputData} name="serviceQuality">
                                    <option value={1}>1</option>
                                    <option value={2}>2</option>
                                    <option value={3}>3</option>
                                    <option value={4}>4</option>
                                    <option value={5}>5</option>
                                </select>
                            </label>
                            <label className="ratingSelector">
                                Interior:
                                <select value={rating.interior} onChange={changeInputData} name="interior">
                                    <option value={1}>1</option>
                                    <option value={2}>2</option>
                                    <option value={3}>3</option>
                                    <option value={4}>4</option>
                                    <option value={5}>5</option>
                                </select>
                            </label>
                            <button
                                className="uploadImgBtn"
                                onClick={addReviewHandler}
                            >
                                Submit review
                            </button>
                        </div>
                    }
                </div>
                {user &&
                    <>
                        <h3>Add new photography: </h3>
                        <div className="imageBlock">
                            <input onChange={handleImg} type="file" className="inputForImages" id="inputImage"/>
                            <label className="inputImage" htmlFor="inputImage">Choose file</label>
                        </div>
                        {image !== "" &&
                        <div className="choosenImage">
                            <img src={image} className="choosenImg" alt=""/>
                        </div>
                        }
                        {image !== "" &&
                        <button
                            className="uploadImgBtn"
                            type="button" onClick={updateGalleryHandler}
                        >
                            Upload
                        </button>
                        }
                    </>
                }
            </div>
        </div>
    );
};

export default Place;
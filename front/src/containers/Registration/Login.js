import React, {useState} from 'react';
import './Registration.css';
import Header from "../../components/Header/Header";
import {useDispatch, useSelector} from "react-redux";
import {loginUser} from "../../store/actions";

const Login = (props) => {
    const errors = useSelector(state => state.user.errors);
    const user = useSelector(state => state.user.user);
    const dispatch = useDispatch();

    if (user) {
        props.history.push("/");
    }

    const [state, setState] = useState({
        username: "",
        password: "",
    });

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const toRegister = () => {
        props.history.push("/register");
    };

    const loginUserSuccess = () => {
        dispatch(loginUser(state));
    };

    return (
        <div>
            <Header/>
            <div className="content">
                <h3 className="regTitle">Login</h3>
                <div className="form">
                    <input
                        type="text" placeholder="Add Username*"
                        className="regFields" name="username"
                        value={state.username} onChange={inputChangeHandler}
                    />
                    <input
                        type="password" placeholder="Add Password*"
                        className="regFields" name="password"
                        value={state.password} onChange={inputChangeHandler}
                    />
                    <button className="addBtn" onClick={loginUserSuccess}>Login</button>
                    {errors && <span className="error">{errors}</span>}
                    <p className="changer" onClick={toRegister}>Create Account</p>
                </div>
            </div>
        </div>
    );
};

export default Login;
import React, {useState} from 'react';
import "./Registration.css"
import Header from "../../components/Header/Header";
import {useDispatch, useSelector} from "react-redux";
import {registerUser} from "../../store/actions";

const Register = props => {
    const errors = useSelector(state => state.user.errors);
    const user = useSelector(state => state.user.user);
    const dispatch = useDispatch();

    if (user) {
        props.history.push("/");
    }

    const [state, setState] = useState({
        username: "",
        fullName: "",
        password: "",
    });

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const toLogin = () => {
        props.history.push("/login");
    };

    const registerUserHandler = () => {
        dispatch(registerUser(state));
    };

    return (
        <div>
            <Header/>
            <div className="content">
                <h3 className="regTitle">Registration</h3>
                <div className="form">
                    <input
                        type="text" placeholder="Add Username*"
                        className="regFields" name="username"
                        value={state.username} onChange={inputChangeHandler}
                    />
                    <input
                        type="text" placeholder="Add fullName*"
                        className="regFields" name="fullName"
                        value={state.fullName} onChange={inputChangeHandler}
                    />
                    <input
                        type="password" placeholder="Create Password*"
                        className="regFields" name="password"
                        value={state.password} onChange={inputChangeHandler}
                    />
                    <button
                        className="addBtn"
                        onClick={registerUserHandler}
                    >Create</button>
                    {errors && <span className="error">{errors}</span>}
                    <p className="changer" onClick={toLogin}>Already have an account? Login!</p>
                </div>
            </div>
        </div>
    );
};

export default Register;
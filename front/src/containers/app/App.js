import React from 'react';
import './App.css';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import MainPage from "../MainPage/MainPage";
import Register from "../Registration/Register";
import Login from "../Registration/Login";
import AddNewPlace from "../AddNewPlace/AddNewPlace";
import Place from "../Place/Place";

const App = () => {
  return (
      <div className="container">
          <BrowserRouter>
              <Switch>
                  <Route path="/" exact component={MainPage}/>
                  <Route path="/register" component={Register}/>
                  <Route path="/login" component={Login}/>
                  <Route path="/addNewPlace" component={AddNewPlace}/>
                  <Route path="/place/:id" component={Place}/>
              </Switch>
          </BrowserRouter>
      </div>
  );
};

export default App;

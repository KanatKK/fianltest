import axiosApi from "../axiosApi";
import {
    ADD_PLACE_SUCCESS,
    GET_ALL_PLACES,
    GET_PLACE,
    LOGIN_USER_SUCCESS,
    LOGOUT_USER, PLACE_FAILURE,
    REGISTER_USER_SUCCESS,
    USER_FAILURE
} from "./actionTypes";

//USER ACTIONS:

const registerSuccess = user => {
    return {type: REGISTER_USER_SUCCESS, user};
};

const loginSuccess = user => {
    return {type: LOGIN_USER_SUCCESS, user};
};

const logoutUser = () => {
    return {type: LOGOUT_USER};
};

const failures = error => {
    return {type: USER_FAILURE, error};
};

export const registerUser = userData => {
    return async dispatch => {
        try {
            const response = await axiosApi.post('/users', userData);
            dispatch(registerSuccess(response.data));
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(failures(e.response.data.error));
            }
        }
    };
};

export const loginUser = userData => {
    return async dispatch => {
        try {
            const response = await axiosApi.post('/users/sessions', userData);
            dispatch(loginSuccess(response.data));
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(failures(e.response.data.error));
            }
        }
    };
};

export const logOutUser = () => {
    return async dispatch => {
        await axiosApi.delete('/users/sessions');
        dispatch(logoutUser());
    };
};

//PLACE ACTIONS

const getPlacesSuccess = (value) => {
    return {type: GET_ALL_PLACES, value}
};

const getPlaceSuccess = (value) => {
    return {type: GET_PLACE, value};
};

const placesFailure = (error) => {
    return {type: PLACE_FAILURE, error};
};

const addPlaceSuccess = () => {
    return {type: ADD_PLACE_SUCCESS}
}

export const getPlaces = () => {
    return async dispatch => {
        try {
            const response = await axiosApi.get('/places');
            dispatch(getPlacesSuccess(response.data))
        } catch (e) {
            console.log(e);
        }
    };
};

export const getPlace = (id) => {
    return async dispatch => {
        try {
            const response = await axiosApi.get('/places/' + id);
            dispatch(getPlaceSuccess(response.data));
        } catch (e) {
            console.log(e);
        }
    };
};

export const addPlace = (placeData) => {
    return async dispatch => {
        try {
            await axiosApi.post('/places', placeData);
            dispatch(addPlaceSuccess());
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(placesFailure(e.response.data.error));
            }
        }
    };
};

export const updateGallery = (id, img) => {
    return async () => {
        try{
            await axiosApi.patch('/places/' + id, img);
        } catch (e) {
            console.log(e);
        }
    };
};

export const addReview = (id, data) => {
    return async () => {
        try {
            await axiosApi.patch('/places/review/' + id, data);
        } catch (e) {
            console.log(e);
        }
    };
};


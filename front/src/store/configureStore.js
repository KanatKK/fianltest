import {applyMiddleware, combineReducers, createStore} from "redux";
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";
import thunkMiddleware from "redux-thunk";
import user from "./reducers/userReducer";
import place from "./reducers/placesReducer";

const rootReducers = combineReducers({
    user: user,
    place: place,
});

const persistedState = loadFromLocalStorage();

const store = createStore(rootReducers, persistedState, applyMiddleware(thunkMiddleware));

store.subscribe(() => {
    saveToLocalStorage({
        user: {
            user: store.getState().user.user
        }
    });
});

export default store;
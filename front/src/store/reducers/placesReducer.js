import {ADD_PLACE_SUCCESS, GET_ALL_PLACES, GET_PLACE, PLACE_FAILURE} from "../actionTypes";

const initialState = {
    places: null,
    onePlace: null,
    error: "",
    successMessage: "",
};

const place = (state = initialState, action) => {
    switch (action.type) {
        case ADD_PLACE_SUCCESS:
            return {...state, error: ""};
        case PLACE_FAILURE:
            return {...state, error: action.error};
        case GET_PLACE:
            return {...state, onePlace: action.value};
        case GET_ALL_PLACES:
            return {...state, places: action.value};
        default:
            return state;
    }
};

export default place;